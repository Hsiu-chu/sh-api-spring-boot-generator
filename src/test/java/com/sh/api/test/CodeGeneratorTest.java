package com.sh.api.test;

import com.sh.api.generator.CodeGenerator;

/**
 * 代码生成器，根据数据表名称生成对应的Model、Mapper、Service、Controller简化开发。
 */
public class CodeGeneratorTest {
    //JDBC配置，请修改为你项目的实际配置
    private static  final String JDBC_URL = "jdbc:mysql://192.168.3.65:3306/demo?nullCatalogMeansCurrent=true";
    private static final String JDBC_USERNAME = "root";
    private static final String JDBC_PASSWORD = "123456";
    private static final String SELF_PROJECT_PACKAGE_NAME = "demo1";
    public static void main(String[] args) {
        CodeGenerator gen = new CodeGenerator(JDBC_URL,JDBC_USERNAME,JDBC_PASSWORD,SELF_PROJECT_PACKAGE_NAME);
//        gen.genCode("输入表名");
        gen.genCodeByCustomModelName("t_u_user","User");
    }

}
