package ${baseProjectPackage}.service;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${baseProjectPackage}.model.${modelNameUpperCamel}DetailInput;
import ${baseProjectPackage}.model.${modelNameUpperCamel}ListInput;
import ${basePackage}.core.Service;
import ${basePackage}.core.Result;


/**
 * @author ${author}
 * @date ${date}
 */
public interface ${modelNameUpperCamel}Service extends Service<${modelNameUpperCamel}> {
   public Result findDetailByInput(${modelNameUpperCamel}DetailInput input);
   public Result findListByInput(${modelNameUpperCamel}ListInput input);
}
