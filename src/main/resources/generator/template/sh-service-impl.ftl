package ${baseProjectPackage}.service.impl;

import ${baseProjectPackage}.dao.${modelNameUpperCamel}Mapper;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${baseProjectPackage}.model.${modelNameUpperCamel}DetailInput;
import ${baseProjectPackage}.model.${modelNameUpperCamel}ListInput;
import ${baseProjectPackage}.service.${modelNameUpperCamel}Service;
import ${basePackage}.core.AbstractService;
import ${basePackage}.core.Result;
import ${basePackage}.core.ResultGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;

import java.util.List;


/**
 * @author ${author}
 * @date ${date}
 */
@Service
@Transactional(readOnly = true)
public class ${modelNameUpperCamel}ServiceImpl extends AbstractService<${modelNameUpperCamel}> implements ${modelNameUpperCamel}Service {
    @Resource
    private ${modelNameUpperCamel}Mapper ${modelNameLowerCamel}Mapper;

    @Override
    public Result findDetailByInput(${modelNameUpperCamel}DetailInput input) {
        ${modelNameUpperCamel} cond = handleCondByParam(input);
        ${modelNameUpperCamel} bean = ${modelNameLowerCamel}Mapper.selectOne(cond);
        if(bean != null){
            return ResultGenerator.genSuccessResult(bean);
        }else {
            return ResultGenerator.genFailResult();
        }
    }
    /**
    * 根据请求参数组装条件实体类
    * @param input
    * @return
    */
    private ${modelNameUpperCamel} handleCondByParam(${modelNameUpperCamel}DetailInput input) {
        ${modelNameUpperCamel} cond = new ${modelNameUpperCamel}();
        cond.setId(input.getId());
        if(StringUtils.isNotEmpty(input.getName())){
            cond.setName(input.getName());
        }
        return  cond;
    }
    @Override
    public Result findListByInput(${modelNameUpperCamel}ListInput input) {
        PageHelper.startPage(input.getPageNum(), input.getPageSize());
        ${modelNameUpperCamel} cond = handleCondByParam(input);
        List<${modelNameUpperCamel}>  list = ${modelNameLowerCamel}Mapper.select(cond);
        PageInfo pageInfo = new PageInfo(list);
        if(pageInfo.getTotal()>0){
            return ResultGenerator.genSuccessResult(pageInfo.getList(),pageInfo.getTotal());
        }else {
            return ResultGenerator.genFailResult();
        }
    }

    /**
    * 根据请求参数组装条件实体类
    * @param input
    * @return
    */
    private ${modelNameUpperCamel} handleCondByParam(${modelNameUpperCamel}ListInput input) {
        ${modelNameUpperCamel} cond = new ${modelNameUpperCamel}();
        return  cond;
    }
}