package ${baseProjectPackage}.service;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${basePackage}.core.Service;


/**
 * Created by ${author} on ${date}.
 */
public interface ${modelNameUpperCamel}Service extends Service<${modelNameUpperCamel}> {

}
