package ${baseProjectPackage}.web;
import ${basePackage}.core.Result;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${baseProjectPackage}.model.${modelNameUpperCamel}DetailInput;
import ${baseProjectPackage}.model.${modelNameUpperCamel}ListInput;
import ${baseProjectPackage}.service.${modelNameUpperCamel}Service;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import com.sh.log.aop.SaveLog;

/**
* @author ${author}
* @date ${date}
*/
@RestController
@RequestMapping("${baseRequestMapping}")
public class ${modelNameUpperCamel}Controller {
    @Resource
    private ${modelNameUpperCamel}Service ${modelNameLowerCamel}Service;

    @PostMapping(value="/detail",consumes=MediaType.APPLICATION_JSON_VALUE)
    @SaveLog
    public Result detail(@RequestBody @Valid  ${modelNameUpperCamel}DetailInput input) {
        return ${modelNameLowerCamel}Service.findDetailByInput(input);
    }

    @PostMapping(value="/list",consumes=MediaType.APPLICATION_JSON_VALUE)
    @SaveLog
    public Result list(@RequestBody @Valid  ${modelNameUpperCamel}ListInput input) {
        return ${modelNameLowerCamel}Service.findListByInput(input);
    }
}
