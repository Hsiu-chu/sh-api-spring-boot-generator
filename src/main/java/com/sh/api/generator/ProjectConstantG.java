package com.sh.api.generator;


/**
 * 项目常量
 */
public final class ProjectConstantG {
    public static final String PROJECT_NAME = "shdemo";//项目名称占位符
    public static final String BASE_PACKAGE = "com.sh.api";//代码所在的基础包名称，可根据自己公司的项目修改（注意：这个配置修改之后需要手工修改src目录项目默认的包路径，使其保持一致，不然会找不到类）
    public static final String PROJECT_NAME_PACKAGE = "." + PROJECT_NAME;//项目包名
    public static final String PROJECT_BASE_PACKAGE = BASE_PACKAGE + PROJECT_NAME_PACKAGE;//项目基本包
    public static final String MODEL_PACKAGE = PROJECT_BASE_PACKAGE + ".model";//生成的Model所在包
    public static final String MAPPER_PACKAGE = PROJECT_BASE_PACKAGE + ".dao";//生成的Mapper所在包
    public static final String SERVICE_PACKAGE = PROJECT_BASE_PACKAGE + ".service";//生成的Service所在包
    public static final String SERVICE_IMPL_PACKAGE = SERVICE_PACKAGE + ".impl";//生成的ServiceImpl所在包
    public static final String CONTROLLER_PACKAGE = PROJECT_BASE_PACKAGE + ".web";//生成的controller所在包
    public static final String MAPPER_INTERFACE_REFERENCE = BASE_PACKAGE + ".core.Mapper";//Mapper插件基础接口的完全限定名
}
